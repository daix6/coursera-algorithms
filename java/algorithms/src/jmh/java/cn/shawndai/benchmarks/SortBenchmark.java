package cn.shawndai.benchmarks;

import cn.shawndai.algorithms.Sort;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Benchmark)
public class SortBenchmark {
  @Param({"1000", "10000"})
  private int size;

  public Integer[] generateRandomArray() {
    Integer[] array = new Integer[size];
    Random random = new Random();

    for (int i = 0; i < size; i++) {
      array[i] = random.nextInt(size);
    }

    return array;
  }

  @Benchmark
  public void testGenerateRandomArray() {
    generateRandomArray();
  }

  @Benchmark
  public void testSelection() {
    Integer[] array = generateRandomArray();
    Sort.selection(array);
  }

  @Benchmark
  public void testInsertion() {
    Integer[] array = generateRandomArray();
    Sort.insertion(array);
  }

  @Benchmark
  public void testShellsort() {
    Integer[] array = generateRandomArray();
    Sort.shellsort(array);
  }
}
