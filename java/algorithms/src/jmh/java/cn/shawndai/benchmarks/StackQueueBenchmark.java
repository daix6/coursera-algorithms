package cn.shawndai.benchmarks;

import cn.shawndai.ds.Queue;
import cn.shawndai.ds.QueueInArray;
import cn.shawndai.ds.StackInArray;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class StackQueueBenchmark {

    @State(Scope.Benchmark)
    public static class MyState {
        public int[] operations;

        @Setup(Level.Trial)
        public void setUp() {
            operations = new int[6];

            Random random = new Random();
            operations[0] = random.nextInt(500);
            operations[1] = random.nextInt(operations[0]);
            operations[2] = random.nextInt(1000);
            operations[3] = random.nextInt(operations[2]);
            operations[4] = random.nextInt(2000);
            operations[5] = random.nextInt(operations[4]);
        }
    }

    @Benchmark
    public void testStackPush() {
        Stack<String> stack = new Stack<>();
        stack.push("A");
    }

    @Benchmark
    public void testStackPop() {
        Stack<String> stack = new Stack<>();
        stack.push("A");
        stack.pop();
    }

    @Benchmark
    public void testStackInArrayPush() {
        StackInArray<String> stack = new StackInArray<>();
        stack.push("A");
    }

    @Benchmark
    public void testStackInArrayPop() {
        StackInArray<String> stack = new StackInArray<>();
        stack.push("A");
        stack.pop();
    }

    @Benchmark
    public void testQueueEnque() {
        Queue<String> queue = new Queue<>();
        queue.enqueue("A");
    }

    @Benchmark
    public void testQueueDeque() {
        Queue<String> queue = new Queue<>();
        queue.enqueue("A");
        queue.dequeue();
    }

    @Benchmark
    public void testQueueInArrayEnque() {
        QueueInArray<String> queue = new QueueInArray<>();
        queue.enqueue("A");
    }

    @Benchmark
    public void testQueueInArrayDeque() {
        QueueInArray<String> queue = new QueueInArray<>();
        queue.enqueue("A");
        queue.dequeue();
    }

    @Benchmark
    public void testStackBigOperations(MyState state) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < state.operations[0]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[1]; i++) {
            stack.pop();
        }
        for (int i = 0; i < state.operations[2]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[3]; i++) {
            stack.pop();
        }
        for (int i = 0; i < state.operations[4]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[5]; i++) {
            stack.pop();
        }
    }

    @Benchmark
    public void testStackInArrayBigOperations(MyState state) {
        StackInArray<Integer> stack = new StackInArray<>();
        for (int i = 0; i < state.operations[0]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[1]; i++) {
            stack.pop();
        }
        for (int i = 0; i < state.operations[2]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[3]; i++) {
            stack.pop();
        }
        for (int i = 0; i < state.operations[4]; i++) {
            stack.push(1);
        }
        for (int i = 0; i < state.operations[5]; i++) {
            stack.pop();
        }
    }

    @Benchmark
    public void testQueueBigOperations(MyState state) {
        Queue<Integer> queue = new Queue<>();
        for (int i = 0; i < state.operations[0]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[1]; i++) {
            queue.dequeue();
        }
        for (int i = 0; i < state.operations[2]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[3]; i++) {
            queue.dequeue();
        }
        for (int i = 0; i < state.operations[4]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[5]; i++) {
            queue.dequeue();
        }
    }

    @Benchmark
    public void testQueueInArrayBigOperations(MyState state) {
        QueueInArray<Integer> queue = new QueueInArray<>();
        for (int i = 0; i < state.operations[0]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[1]; i++) {
            queue.dequeue();
        }
        for (int i = 0; i < state.operations[2]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[3]; i++) {
            queue.dequeue();
        }
        for (int i = 0; i < state.operations[4]; i++) {
            queue.enqueue(1);
        }
        for (int i = 0; i < state.operations[5]; i++) {
            queue.dequeue();
        }
    }
}
