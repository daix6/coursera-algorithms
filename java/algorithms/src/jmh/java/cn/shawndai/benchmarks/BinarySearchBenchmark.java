package cn.shawndai.benchmarks;

import cn.shawndai.algorithms.BinarySearch;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class BinarySearchBenchmark {
  @State(Scope.Benchmark)
  public static class MyState {
    public int[] array;
    public Random random;

    @Setup(Level.Trial)
    public void setup() {
      random = new Random();

      array = new int[random.nextInt(100) + 2];
      for (int i = 0; i < array.length; i++) {
        array[i] = random.nextInt();
      }
      Arrays.sort(array);
    }

    @TearDown(Level.Trial)
    public void teardown() {
      array = null;
      random = null;
    }
  }

  @Benchmark
  public void testSearchExisted(MyState state) {
    BinarySearch.search(state.array, state.random.nextInt(state.array.length));
  }

  @Benchmark
  public void testSearchNotExisted(MyState state) {
    BinarySearch.search(state.array, state.array[0] - 1);
  }
}
