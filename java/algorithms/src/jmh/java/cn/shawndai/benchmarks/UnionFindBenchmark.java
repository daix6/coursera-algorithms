package cn.shawndai.benchmarks;

import cn.shawndai.algorithms.QuickFind;
import cn.shawndai.algorithms.QuickUnion;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class UnionFindBenchmark {
  @State(Scope.Benchmark)
  public static class MyState {
    @Param({"100", "10000"})
    public int capacity;
    @Param({"3600", "400000"})
    public int unions;
    public int p;
    public int q;

    @Setup(Level.Trial)
    public void setup() {
      p = Math.abs(new Random().nextInt(capacity));
      q = Math.abs(new Random().nextInt(capacity));
    }
  }

  public QuickFind generateQuickFind(int capacity, int unions) {
    QuickFind qf = new QuickFind(capacity);

    Random r1 = new Random();
    Random r2 = new Random();

    for (int i = 0; i < unions; i++) {
      qf.union(Math.abs(r1.nextInt(capacity)), Math.abs(r2.nextInt(capacity)));
    }

    return qf;
  }

  public QuickUnion generateQuickUnion(int capacity, int unions) {
    QuickUnion qu = new QuickUnion(capacity);

    Random r1 = new Random();
    Random r2 = new Random();

    for (int i = 0; i < unions; i++) {
      qu.union(r1.nextInt(capacity), r2.nextInt(capacity));
    }

    return qu;
  }

  public QuickUnion generateQuickUnionWeightedBySize(int capacity, int unions) {
    QuickUnion qu = new QuickUnion(capacity);

    Random r1 = new Random();
    Random r2 = new Random();

    for (int i = 0; i < unions; i++) {
      qu.weightedUnionBySize(r1.nextInt(capacity), r2.nextInt(capacity));
    }

    return qu;
  }

  public QuickUnion generateQuickUnionWeightedByDepth(int capacity, int unions) {
    QuickUnion qu = new QuickUnion(capacity);

    Random r1 = new Random();
    Random r2 = new Random();

    for (int i = 0; i < unions; i++) {
      qu.weightedUnionByDepth(r1.nextInt(capacity), r2.nextInt(capacity));
    }

    return qu;
  }

  @Benchmark
  public void testGenerateQuickFind(MyState state) {
    generateQuickFind(state.capacity, state.unions);
  }

  @Benchmark
  public void testGenerateQuickUnion(MyState state) {
    generateQuickUnion(state.capacity, state.unions);
  }

  @Benchmark
  public void testGenerateQuickUnionWeightedBySize(MyState state) {
    generateQuickUnionWeightedBySize(state.capacity, state.unions);
  }

  @Benchmark
  public void testGenerateQuickUnionWeightedByDepth(MyState state) {
    generateQuickUnionWeightedByDepth(state.capacity, state.unions);
  }

  @Benchmark
  public void testQuickFindUnion(MyState state) {
    QuickFind qf = generateQuickFind(state.capacity, state.unions);
    qf.union(state.p, state.q);
  }

  @Benchmark
  public void testQuickFindConnected(MyState state) {
    QuickFind qf = generateQuickFind(state.capacity, state.unions);
    qf.connected(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionUnion(MyState state) {
    QuickUnion qu = generateQuickUnion(state.capacity, state.unions);
    qu.union(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionConnected(MyState state) {
    QuickUnion qu = generateQuickUnion(state.capacity, state.unions);
    qu.connected(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionWeightedBySizeUnion(MyState state) {
    QuickUnion qu = generateQuickUnionWeightedBySize(state.capacity, state.unions);
    qu.union(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionWeightedBySizeConnected(MyState state) {
    QuickUnion qu = generateQuickUnionWeightedBySize(state.capacity, state.unions);
    qu.connected(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionWeightedByDepthUnion(MyState state) {
    QuickUnion qu = generateQuickUnionWeightedByDepth(state.capacity, state.unions);
    qu.union(state.p, state.q);
  }

  @Benchmark
  public void testQuickUnionWeightedByDepthConnected(MyState state) {
    QuickUnion qu = generateQuickUnionWeightedByDepth(state.capacity, state.unions);
    qu.connected(state.p, state.q);
  }
}
