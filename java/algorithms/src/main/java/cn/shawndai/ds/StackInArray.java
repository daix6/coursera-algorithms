package cn.shawndai.ds;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class StackInArray<ItemT> implements Iterable<ItemT> {
  private ItemT[] array;
  private int size;

  /** Constructor. */
  public StackInArray() {
    array = (ItemT[])new Object[1];
    size = 0;
  }

  /**
   * Insert {@code item} to stack's end.
   * @param item to insert
   */
  public void push(ItemT item) {
    if (array.length == size) {
      resize(array.length * 2);
    }
    array[size++] = item;
  }

  /**
   * Remove the top item in the stack.
   * @return removed item
   */
  public ItemT pop() {
    if (isEmpty()) {
      throw new NoSuchElementException("Stack underflow!");
    }
    ItemT item = array[--size];
    array[size] = null;

    if (size == array.length / 4) {
      resize(array.length / 2);
    }
    return item;
  }

  /** Return the top item in the stack. */
  public ItemT peek() {
    if (isEmpty()) {
      throw new NoSuchElementException("Stack underflow!");
    }
    return array[size - 1];
  }

  /** Return whether the stack is empty or not. */
  public boolean isEmpty() {
    return size == 0;
  }

  /** Return stack's size. */
  public int size() {
    return size;
  }

  /** Resize the array stores items to {@code target}.
   * @param target the target array size
   */
  private void resize(int target) {
    if (target == 0) {
      return;
    }
    ItemT[] array = (ItemT[])new Object[target];
    for (int i = 0; i < this.size; i++) {
      array[i] = this.array[i];
    }
    this.array = array;
  }

  @Override
  public Iterator<ItemT> iterator() {
    return new StackIterator<ItemT>(this.array, this.size);
  }

  private class StackIterator<ItemT> implements Iterator<ItemT> {
    public ItemT[] array;
    public int current;

    public StackIterator(ItemT[] array, int size) {
      this.array = array;
      this.current = size;
    }

    @Override
    public boolean hasNext() {
      return current > 0;
    }

    @Override
    public ItemT next() {
      if (hasNext()) {
        return array[--current];
      }
      throw new NoSuchElementException();
    }
  }
}
