package cn.shawndai.ds;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<ItemT> implements Iterable<ItemT> {
  private class Node<ItemT> {
    public ItemT item;
    public Node<ItemT> next;

    public Node(ItemT item, Node node) {
      this.item = item;
      this.next = node;
    }
  }

  private Node<ItemT> first;
  private Node<ItemT> last;
  private int size;

  /** Constructor. */
  public Queue() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  /**
   * Insert {@code item} to queue's end.
   * @param item to insert
   */
  public void enqueue(ItemT item) {
    Node<ItemT> newLast = new Node<ItemT>(item, null);
    if (isEmpty()) {
      first = newLast;
      last = newLast;
    } else {
      last.next = newLast;
      last = newLast;
    }
    size++;
  }

  /**
   * Remove the first item in queue.
   * @return removed item
   */
  public ItemT dequeue() {
    if (first == null) {
      throw new NoSuchElementException("Queue underflow!");
    }

    final ItemT item = first.item;
    first = first.next;
    if (isEmpty()) {
      last = null;
    }
    size--;
    return item;
  }

  /** Return the first item in queue. */
  public ItemT peek() {
    if (first == null) {
      throw new NoSuchElementException("Queue underflow!");
    }
    return first.item;
  }

  /** Return whether the queue is empty or not. */
  public boolean isEmpty() {
    return first == null;
  }

  /** Return queue's size. */
  public int size() {
    return size;
  }

  @Override
  public Iterator<ItemT> iterator() {
    return new QueueIterator<>(first);
  }

  private class QueueIterator<ItemT> implements Iterator<ItemT> {
    private Node<ItemT> current;

    public QueueIterator(Node first) {
      this.current = first;
    }

    @Override
    public boolean hasNext() {
      return current != null;
    }

    @Override
    public ItemT next() {
      if (hasNext()) {
        ItemT item = current.item;
        current = current.next;
        return item;
      }

      throw new NoSuchElementException("Queue underflow!");
    }
  }
}
