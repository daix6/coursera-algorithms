package cn.shawndai.ds;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<ItemT> implements Iterable<ItemT> {
  private class Node<ItemT> {
    public ItemT item;
    public Node next;

    public Node(ItemT item, Node<ItemT> node) {
      this.item = item;
      this.next = node;
    }
  }

  private Node<ItemT> top = null;
  private int size = 0;

  /**
   * Insert {@code item} to stack's end.
   * @param item to insert
   */
  public void push(ItemT item) {
    Node node = new Node<ItemT>(item, top);

    top = node;
    size++;
  }

  /**
   * Remove the top item in the stack.
   * @return removed item
   */
  public ItemT pop() {
    if (top == null) {
      throw new NoSuchElementException("Stack underflow!");
    }
    ItemT item = top.item;
    top = top.next;
    size--;

    return item;
  }

  /** Return the top item in the stack. */
  public ItemT peek() {
    if (top == null) {
      throw new NoSuchElementException("Stack underflow!");
    }

    return top.item;
  }

  /** Return whether the stack is empty or not. */
  public boolean isEmpty() {
    return top == null;
  }

  /** Return stack's size. */
  public int size() {
    return size;
  }

  @Override
  public Iterator<ItemT> iterator() {
    return new StackIterator<ItemT>(top);
  }

  private class StackIterator<ItemT> implements Iterator<ItemT> {
    private Node<ItemT> current;

    public StackIterator(Node<ItemT> top) {
      current = top;
    }

    @Override
    public ItemT next() {
      if (hasNext()) {
        ItemT item = current.item;
        current = current.next;
        return item;
      }
      throw new NoSuchElementException();
    }

    @Override
    public boolean hasNext() {
      return current != null;
    }
  }
}
