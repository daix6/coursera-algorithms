package cn.shawndai.ds;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class QueueInArray<ItemT> implements Iterable<ItemT> {
  private ItemT[] array;
  private int first;
  private int last; // One index behind the last element
  private int size;

  /** Constructor. */
  public QueueInArray() {
    this.array = (ItemT[])new Object[1];
    this.first = -1;
    this.last = -1;
    this.size = 0;
  }

  /**
   * Insert {@code item} to queue's end.
   * @param item to insert
   */
  public void enqueue(ItemT item) {
    if (size == array.length) {
      resize(array.length * 2);
    }
    if (isEmpty()) {
      first = 0;
      last = 0;
    } else {
      last = (last + 1) % array.length;
    }
    array[last] = item;
    size++;
  }

  /**
   * Remove the first item in queue.
   * @return removed item
   */
  public ItemT dequeue() {
    if (isEmpty()) {
      throw new NoSuchElementException("Queue underflow");
    }
    final ItemT item = array[first];
    size--;

    if (isEmpty()) {
      first = -1;
      last = -1;
    } else {
      first = (first + 1) % array.length;
    }

    if (size == array.length / 4) {
      resize(array.length / 2);
    }

    return item;
  }

  /** Return the first item in queue. */
  public ItemT peek() {
    if (isEmpty()) {
      throw new NoSuchElementException("Queue underflow!");
    }

    return array[first];
  }

  /** Return whether the queue is empty or not. */
  public boolean isEmpty() {
    return size == 0;
  }

  /** Return queue's size. */
  public int size() {
    return size;
  }

  /** Resize the array stores items to {@code target}.
   * @param target the target array size
   */
  private void resize(int target) {
    if (target == 0) {
      return;
    }
    ItemT[] array = (ItemT[])new Object[target];

    if (first <= last) {
      for (int i = first, j = 0; i <= last; i++, j++) {
        array[j] = this.array[i];
      }
    } else {
      int j = 0;
      for (int i = first; i < this.array.length; i++, j++) {
        array[j] = this.array[i];
      }
      for (int i = 0; i <= last; i++, j++) {
        array[j] = this.array[i];
      }
    }

    last = (last - first + this.array.length) % this.array.length;
    first = 0;
    this.array = array;
  }

  @Override
  public Iterator<ItemT> iterator() {
    return new QueueIterator<ItemT>(array, first, size);
  }

  private class QueueIterator<ItemT> implements Iterator<ItemT> {
    private ItemT[] array;
    private int current;
    private int size;
    private int count;

    public QueueIterator(ItemT[] array, int first, int size) {
      this.array = array;
      this.current = first;
      this.size = size;
      this.count = 0;
    }

    @Override
    public boolean hasNext() {
      return count < size;
    }

    @Override
    public ItemT next() {
      if (hasNext()) {
        ItemT item = array[current];
        current = (current + 1) % array.length;
        count++;
        return item;
      }

      throw new NoSuchElementException();
    }
  }
}
