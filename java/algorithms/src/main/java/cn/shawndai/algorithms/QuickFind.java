package cn.shawndai.algorithms;

public class QuickFind {
  private int[] ids;
  private int componentsCount;

  /** Constructor. */
  public QuickFind(int length) {
    ids = new int[length];
    componentsCount = length;

    for (int i = 0; i < length; i++) {
      ids[i] = i;
    }
  }

  /** Union p and q. */
  public void union(int p, int q) {
    int componentP = ids[p];
    int componentQ = ids[q];

    if (componentP == componentQ) {
      return;
    }

    for (int i = 0; i < ids.length; i++) {
      if (ids[i] == componentP) {
        ids[i] = componentQ;
      }
    }
    
    componentsCount--;
  }

  /** Return the component id of p. */
  public int find(int p) {
    return ids[p];
  }

  /** Return components count. */
  public int count() {
    return componentsCount;
  }

  /** Return whether p and q are connected. */
  public boolean connected(int p, int q) {
    return ids[p] == ids[q];
  }
}