package cn.shawndai.algorithms;

public class Sort {
  /**
   * 选择排序，每次从右侧（未排序部分）选择最小的元素与右侧第一个元素交换位置。
   * 选择排序的时间复杂度总是 O(N^2)。
   * @param array to sort
   */
  public static void selection(Comparable[] array) {
    for (int i = 0; i < array.length; i++) {
      int minIndex = i;
      for (int j = i + 1; j < array.length; j++) {
        if (less(array[j], array[minIndex])) {
          minIndex = j;
        }
      }
      swap(array, i, minIndex);
    }
  }

  /**
   * 插入排序，每次从右侧（未排序部分）选取头部元素不断与其左侧元素比较与交换位置。
   * 插入排序的时间复杂度依赖于数组的排序状态，最佳情况 O(N)，最坏情况 O(N^2)。
   * 对部分排序好的数组，插入排序几乎可以达到线性的时间复杂度。
   * @param array to sort
   */
  public static void insertion(Comparable[] array) {
    for (int i = 1; i < array.length; i++) {
      for (int j = i; j > 0; j--) {
        if (less(array[j], array[j - 1])) {
          swap(array, j, j - 1);
        } else {
          break;
        }
      }
    }
  }

  /**
   * 希尔排序，其实是利用插入排序在部分排序的数组的良好表现实现的一种算法。
   * 时间复杂度据说是 O(N^(3/2))，难点在找出合适的步长算法。
   * @param array to sort
   */
  public static void shellsort(Comparable[] array) {
    int h = 1;
    while (h < array.length / 3) {
      h = 3 * h + 1;
    }

    while (h >= 1) {
      for (int i = h; i < array.length; i++) {
        for (int j = i; j >= h; j -= h) {
          if (less(array[j], array[j - h])) {
            swap(array, j, j - h);
          } else {
            break;
          }
        }
      }
      h /= 3;
    }
  }

  /** Utility that compare a and b. */
  public static boolean less(Comparable a, Comparable b) {
    return a.compareTo(b) <= 0;
  }

  /** Utility that swap index a and index b in array. */
  public static void swap(Comparable[] array, int a, int b) {
    Comparable temp = array[a];
    array[a] = array[b];
    array[b] = temp;
  }
}
