package cn.shawndai.algorithms;

public class BinarySearch {
  /**
   * Search {@code ele}'s index from {@code arr}.
   * @param arr the array to search which is persumed to be sorted in ascending order
   * @param ele the element to search
   * @return the {@code ele}'s index in {@code arr} if {@code ele} is not in {@code arr},
   *         and -1 if not.
   */
  public static int search(int[] arr, int ele) {
    for (int i = 1; i < arr.length; i++) {
      if (arr[i] < arr[i - 1]) {
        throw new IllegalArgumentException("The array is not sorted");
      }
    }

    int low = 0;
    int high = arr.length - 1;
    int mid;

    while (low <= high) {
      mid = (low + high) / 2;
      if (arr[mid] == ele) {
        return mid;
      } else if (arr[mid] > ele) {
        high = mid - 1;
      } else {
        low = mid + 1;
      }
    }

    return -1;
  }
}
