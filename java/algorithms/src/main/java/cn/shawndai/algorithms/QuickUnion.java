package cn.shawndai.algorithms;

public class QuickUnion {
  private int[] ids;
  private int[] depth;
  private int[] size;
  private int componentsCount;

  /** Constructor with length sites. */
  public QuickUnion(int length) {
    ids = new int[length];
    depth = new int[length];
    size = new int[length];

    for (int i = 0; i < length; i++) {
      ids[i] = i;
      depth[i] = 1;
      size[i] = 1;
    }
    componentsCount = length;
  }

  /** Union p and q. */
  public void union(int p, int q) {
    int rootOfP = root(p);
    int rootOfQ = root(q);
    
    if (rootOfP == rootOfQ) {
      return;
    }

    ids[rootOfP] = rootOfQ;
    componentsCount--;
  }

  /** Find the root of p. */
  public int find(int p) {
    return root(p);
  }

  /** Judge whether p and q is connected. */
  public boolean connected(int p, int q) {
    return root(p) == root(q);
  }

  /** Return the components count. */
  public int count() {
    return componentsCount;
  }

  /** Get the root of p. */
  public int root(int p) {
    while (ids[p] != p) {
      p = ids[p];
    }

    return p;
  }

  /** Union p and q weighted by depth. */
  public void weightedUnionByDepth(int p, int q) {
    int rootOfP = root(p);
    int rootOfQ = root(q);
    
    if (rootOfP == rootOfQ) {
      return;
    }

    if (depth[rootOfP] > depth[rootOfQ]) {
      ids[rootOfQ] = rootOfP; 
    } else if (depth[rootOfP] < depth[rootOfQ]) {
      ids[rootOfP] = rootOfQ;
    } else {
      ids[rootOfP] = rootOfQ;
      depth[rootOfQ]++;
    }

    componentsCount--;
  }

  /** Union p and q weighted by size. */
  public void weightedUnionBySize(int p, int q) {
    int rootOfP = root(p);
    int rootOfQ = root(q);
    
    if (rootOfP == rootOfQ) {
      return;
    }

    if (size[rootOfP] > size[rootOfQ]) {
      ids[rootOfQ] = rootOfP;
      size[rootOfP] += size[rootOfQ];
    } else {
      ids[rootOfP] = rootOfQ;
      size[rootOfQ] += size[rootOfP];
    }
    componentsCount--;
  }
}