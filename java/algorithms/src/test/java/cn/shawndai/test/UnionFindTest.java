package cn.shawndai.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cn.shawndai.algorithms.QuickFind;
import cn.shawndai.algorithms.QuickUnion;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UnionFindTest {
  private In file;
  private QuickFind quickFind;
  private QuickUnion quickUnion;
  private QuickUnion quickWeightedUnionByDepth;
  private QuickUnion quickWeightedUnionBySize;
  private WeightedQuickUnionUF standardUF;
  private int count;

  @Before
  public void setUp() {
    file = new In(getClass().getClassLoader().getResource("mediumUF.txt").getPath());
    count = file.readInt();
    quickFind = new QuickFind(count);
    quickUnion = new QuickUnion(count);
    quickWeightedUnionByDepth = new QuickUnion(count);
    quickWeightedUnionBySize = new QuickUnion(count);
    standardUF = new WeightedQuickUnionUF(count);
  }

  @After
  public void tearDown() {
    file.close();
  }

  @Test
  public void testUnionAndConnected() {
    while (!file.isEmpty()) {
      int p = file.readInt();
      int q = file.readInt();
      quickFind.union(p, q);
      quickUnion.union(p, q);
      quickWeightedUnionByDepth.weightedUnionByDepth(p, q);
      quickWeightedUnionBySize.weightedUnionBySize(p, q);
      standardUF.union(p, q);

      assertTrue(quickFind.connected(p, q));
      assertEquals(quickFind.find(p), quickFind.find(q));

      assertTrue(quickUnion.connected(p, q));
      assertEquals(quickUnion.find(p), quickUnion.find(q));
      
      assertTrue(quickWeightedUnionByDepth.connected(p, q));
      assertEquals(quickWeightedUnionByDepth.find(p), quickWeightedUnionByDepth.find(q));

      assertTrue(quickWeightedUnionBySize.connected(p, q));
      assertEquals(quickWeightedUnionBySize.find(p), quickWeightedUnionBySize.find(q));
    }

    assertEquals(standardUF.count(), quickWeightedUnionBySize.count());
  }

  @Test
  public void testCount() {
    // 没找到好方法测试...
    assertEquals(quickFind.count(), count);
    assertEquals(quickUnion.count(), count);
    assertEquals(quickWeightedUnionByDepth.count(), count);
    assertEquals(quickWeightedUnionBySize.count(), count);

    int p = file.readInt();
    int q = file.readInt();

    quickFind.union(p, q);
    quickUnion.union(p, q);
    quickWeightedUnionByDepth.weightedUnionByDepth(p, q);
    quickWeightedUnionBySize.weightedUnionBySize(p, q);
    assertEquals(quickFind.count(), count - 1);
    assertEquals(quickUnion.count(), count - 1);
    assertEquals(quickWeightedUnionByDepth.count(), count - 1);
    assertEquals(quickWeightedUnionBySize.count(), count - 1);
  }
}