package cn.shawndai.test;

import static org.junit.Assert.assertEquals;

import cn.shawndai.algorithms.BinarySearch;
import java.util.Arrays;
import java.util.Random;
import org.junit.Before;
import org.junit.Test;

public class BinarySearchTest {
  private int[] array;

  @Before
  public void setup() {
    Random random = new Random();
    int size = random.nextInt(100) + 2;
    array = new int[size];
    for (int i = 0; i < size; i++) {
      array[i] = random.nextInt(Integer.MAX_VALUE);
    }
    Arrays.sort(array);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testThrows() {
    array[1] = array[0] - 10;
    BinarySearch.search(array, 0);
  }

  @Test
  public void testSearchReturnIndex() {
    for (int i = 0; i < array.length; i++) {
      assertEquals(BinarySearch.search(array, array[i]), i);
    }
  }

  @Test
  public void testSearchReturnNegativeOne() {
    assertEquals(BinarySearch.search(array, array[0] - 1), -1);
    assertEquals(BinarySearch.search(array, array[array.length - 1] + 1), -1);
  }
}
