package cn.shawndai.test;

import cn.shawndai.ds.Queue;
import cn.shawndai.ds.QueueInArray;
import cn.shawndai.ds.Stack;
import static org.junit.Assert.assertEquals;

import cn.shawndai.ds.StackInArray;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Random;

public class StackQueueTest {
  @Test
  public void testStackMethods() {
    Stack<String> stack = new Stack<>();

    assertEquals(stack.isEmpty(), true);

    stack.push(".");
    assertEquals(stack.isEmpty(), false);

    stack.push("red");
    assertEquals(stack.size(), 2);
    assertEquals(stack.peek(), "red");
    assertEquals(stack.size(), 2);
    assertEquals(stack.pop(), "red");
    assertEquals(stack.size(), 1);

    stack.push("blue");
    stack.push("looks ");
    stack.push("Sky ");
    assertEquals(stack.size(), 4);

    StringBuilder stringBuilder = new StringBuilder();
    for (String str : stack) {
      stringBuilder.append(str);
    }
    assertEquals(stringBuilder.toString(), "Sky looks blue.");
  }
  @Test(expected = NoSuchElementException.class)
  public void testStackThrows() {
    Stack<int[]> stack = new Stack<>();
    stack.peek();
    stack.pop();
  }
  @Test
  public void testStackInArrayMethods() {
    StackInArray<String> stack = new StackInArray<>();

    assertEquals(stack.isEmpty(), true);

    stack.push(".");
    assertEquals(stack.isEmpty(), false);

    stack.push("red");
    assertEquals(stack.size(), 2);
    assertEquals(stack.peek(), "red");
    assertEquals(stack.size(), 2);
    assertEquals(stack.pop(), "red");
    assertEquals(stack.size(), 1);

    stack.push("blue");
    stack.push("looks ");
    stack.push("Sky ");
    assertEquals(stack.size(), 4);

    StringBuilder stringBuilder = new StringBuilder();
    for (String str : stack) {
      stringBuilder.append(str);
    }
    assertEquals(stringBuilder.toString(), "Sky looks blue.");
  }
  @Test(expected = NoSuchElementException.class)
  public void testStackInArrayThrows() {
    StackInArray<Double> stack = new StackInArray<>();
    stack.push(1.1);
    stack.pop();
    stack.peek();
    stack.pop();
  }

  @Test
  public void testQueueMethods() {
    Queue<String> queue = new Queue<>();

    assertEquals(queue.isEmpty(), true);

    queue.enqueue("Shit ");
    assertEquals(queue.isEmpty(), false);

    queue.enqueue("Sky ");
    queue.enqueue("looks ");
    queue.enqueue("blue");
    assertEquals(queue.size(), 4);
    assertEquals(queue.peek(), "Shit ");
    assertEquals(queue.size(), 4);
    assertEquals(queue.dequeue(), "Shit ");
    assertEquals(queue.size(), 3);

    queue.enqueue(".");
    StringBuilder stringBuilder = new StringBuilder();
    for (String str : queue) {
      stringBuilder.append(str);
    }
    assertEquals(stringBuilder.toString(), "Sky looks blue.");
  }
  @Test(expected = NoSuchElementException.class)
  public void testQueueThrows() {
    Queue<Double> queue = new Queue<>();
    queue.enqueue(1.1);
    queue.dequeue();
    queue.peek();
    queue.dequeue();
  }
  @Test
  public void testQueueInArrayMethods() {
    QueueInArray<String> queue = new QueueInArray<>();

    assertEquals(queue.isEmpty(), true);

    queue.enqueue("Shit ");
    assertEquals(queue.isEmpty(), false);

    queue.enqueue("Sky ");
    queue.enqueue("looks ");
    queue.enqueue("blue");
    assertEquals(queue.size(), 4);
    assertEquals(queue.peek(), "Shit ");
    assertEquals(queue.size(), 4);
    assertEquals(queue.dequeue(), "Shit ");
    assertEquals(queue.size(), 3);

    queue.enqueue(".");
    StringBuilder stringBuilder = new StringBuilder();
    for (String str : queue) {
      stringBuilder.append(str);
    }
    assertEquals(stringBuilder.toString(), "Sky looks blue.");

  }
  @Test(expected = NoSuchElementException.class)
  public void testQueueInArrayThrows() {
    QueueInArray<Double> queue = new QueueInArray<>();
    queue.enqueue(1.1);
    queue.dequeue();
    queue.peek();
    queue.dequeue();
  }

  @Test
  public void testStackBigOperation() {
    int[] array = new int[1000];
    Random random = new Random();
    for (int i = 0; i < 1000; i++) {
      array[i] = random.nextInt(10); // 个位数...
    }

    Stack<Integer> stack = new Stack<>();
    StackInArray<Integer> stackInArray = new StackInArray<>();
    StringBuilder stringBuilder = new StringBuilder();

    int randomNumber = random.nextInt(400) + 1;
    int size = randomNumber; // Stringbuilder size
    int arrayIndex = randomNumber; // Current index of array
    for (int i = 0; i < randomNumber; i++) {
      stack.push(array[i]);
      stackInArray.push(array[i]);
      stringBuilder.append(array[i]);
    }
    randomNumber = random.nextInt(size) + 1;
    stringBuilder.delete(size - randomNumber, size);
    size -= randomNumber;
    for (int i = 0; i < randomNumber; i++) {
      stack.pop();
      stackInArray.pop();
    }

    randomNumber = random.nextInt(600) + 1;
    for (int i = arrayIndex; i < arrayIndex + randomNumber; i++) {
      stack.push(array[i]);
      stackInArray.push(array[i]);
      stringBuilder.append(array[i]);
    }
    arrayIndex += randomNumber;
    size += randomNumber;

    randomNumber = random.nextInt(size) + 1;
    stringBuilder.delete(size - randomNumber, size);
    size -= randomNumber;
    for (int i = 0; i < randomNumber; i++) {
      stack.pop();
      stackInArray.pop();
    }

    for (int i = arrayIndex; i < 1000; i++, size++) {
      stack.push(array[i]);
      stackInArray.push(array[i]);
      stringBuilder.append(array[i]);
    }

    assertEquals(stack.size(), size);
    assertEquals(stackInArray.size(), size);

    stringBuilder.reverse(); // Stack are LIFO
    StringBuilder verify = new StringBuilder();
    for (int str : stack) {
      verify.append(str);
    }
    assertEquals(stringBuilder.toString(), verify.toString());
    verify = new StringBuilder();
    for (int str : stackInArray) {
      verify.append(str);
    }
    assertEquals(stringBuilder.toString(), verify.toString());
  }
  @Test
  public void testQueueBigOperation() {
    int[] array = new int[1000];
    Random random = new Random();
    for (int i = 0; i < 1000; i++) {
      array[i] = random.nextInt(10);
    }

    Queue<Integer> queue = new Queue<>();
    QueueInArray<Integer> queueInArray = new QueueInArray<>();
    StringBuilder stringBuilder = new StringBuilder();

    int randomNumber = random.nextInt(400) + 1;
    int size = randomNumber; // Stringbuilder size
    int arrayIndex = randomNumber; // Current index of array
    for (int i = 0; i < randomNumber; i++) {
      queue.enqueue(array[i]);
      queueInArray.enqueue(array[i]);
      stringBuilder.append(array[i]);
    }
    randomNumber = random.nextInt(size) + 1;
    stringBuilder.delete(0, randomNumber);
    size -= randomNumber;
    for (int i = 0; i < randomNumber; i++) {
      queue.dequeue();
      queueInArray.dequeue();
    }

    randomNumber = random.nextInt(600) + 1;
    for (int i = arrayIndex; i < arrayIndex + randomNumber; i++) {
      queue.enqueue(array[i]);
      queueInArray.enqueue(array[i]);
      stringBuilder.append(array[i]);
    }
    arrayIndex += randomNumber;
    size += randomNumber;

    randomNumber = random.nextInt(size) + 1;
    stringBuilder.delete(0, randomNumber);
    size -= randomNumber;
    for (int i = 0; i < randomNumber; i++) {
      queue.dequeue();
      queueInArray.dequeue();
    }

    for (int i = arrayIndex; i < 1000; i++, size++) {
      queue.enqueue(array[i]);
      queueInArray.enqueue(array[i]);
      stringBuilder.append(array[i]);
    }

    assertEquals(queue.size(), size);
    assertEquals(queueInArray.size(), size);

    // Queue are FIFO
    StringBuilder verify = new StringBuilder();
    for (int str : queue) {
      verify.append(str);
    }
    assertEquals(stringBuilder.toString(), verify.toString());
    verify = new StringBuilder();
    for (int str : queueInArray) {
      verify.append(str);
    }
    assertEquals(stringBuilder.toString(), verify.toString());
  }
}
