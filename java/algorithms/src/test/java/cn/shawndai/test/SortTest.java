package cn.shawndai.test;

import cn.shawndai.algorithms.Sort;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;

public class SortTest {
  private Integer[] array;
  @Before
  public void setUp() {
    Random random = new Random();
    array = new Integer[random.nextInt(100)];
    for (int i = 0; i < array.length; i++) {
      array[i] = random.nextInt(100);
    }
  }

  private boolean isSorted() {
    for (int i = 1; i < array.length; i++) {
      if (array[i] < array[i - 1]) {
        return false;
      }
    }
    return true;
  }

  @Test
  public void testSelection() {
    Sort.selection(array);
    assertTrue(isSorted());
  }

  @Test
  public void testInsertion() {
    Sort.insertion(array);
    assertTrue(isSorted());
  }

  @Test
  public void testShellsort() {
    Sort.shellsort(array);
    assertTrue(isSorted());
  }
}
