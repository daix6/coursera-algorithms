import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;

public class Point implements Comparable<Point> {

  private final int x;     // x-coordinate of this point
  private final int y;     // y-coordinate of this point

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void draw() {
    StdDraw.point(x, y);
  }

  public void drawTo(Point that) {
    StdDraw.line(this.x, this.y, that.x, that.y);
  }

  public double slopeTo(Point that) {
    if (this.x == that.x && this.y == that.y) {
      return Double.NEGATIVE_INFINITY;
    } else if (this.x == that.x) {
      return Double.POSITIVE_INFINITY;
    } else if (this.y == that.y) {
      return 0;
    } else {
      return (that.y - this.y) * 1.0 / (that.x - this.x);
    }
  }

  @Override
  public int compareTo(Point that) {
    if (this.y > that.y) {
      return 1;
    } else if (this.y == that.y) {
      if (this.x > that.x) {
        return 1;
      } else if (this.x < that.x) {
        return -1;
      } else {
        return 0;
      }
    } else {
      return -1;
    }
  }

  public Comparator<Point> slopeOrder() {
      return new PointComparator();
  }

  private class PointComparator implements Comparator<Point> {

    @Override
    public int compare(Point o1, Point o2) {
      double slopeToO1 = slopeTo(o1);
      double slopeToO2 = slopeTo(o2);

      if (slopeToO1 < slopeToO2) {
        return -1;
      } else if (slopeToO1 > slopeToO2) {
        return 1;
      } else {
        return 0;
      }
    }
  }


  public String toString() {
    return "(" + x + ", " + y + ")";
  }

  public static void main(String[] args) {
  }
}