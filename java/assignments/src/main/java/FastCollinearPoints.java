import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {
  private LineSegment[] segments;
  private Point[] points;

  public FastCollinearPoints(Point[] points) {
    if (points == null) throw new IllegalArgumentException();

    for (int i = 0; i < points.length; i++) {
      if (points[i] == null) throw new IllegalArgumentException();
      for (int j = i + 1; j < points.length; j++) {
        if (points[j] == null) throw new IllegalArgumentException();

        if (points[j].compareTo(points[i]) == 0) throw new IllegalArgumentException();
      }
    }

    this.points = points.clone();
    Arrays.sort(this.points);

    ArrayList<LineSegment> segmentsList = new ArrayList<LineSegment>();
    Point[] toSearch;
    ArrayList<Point> subSegments = new ArrayList<Point>();
    int size = this.points.length;

    for (int i = 0; i < size - 3; i++) {
      toSearch = Arrays.copyOfRange(this.points, i + 1, size);
      Arrays.sort(toSearch, this.points[i].slopeOrder());
      Point origin = this.points[i];

      for (int start = 0, j = 1; j < toSearch.length; j++) {
        double slope = origin.slopeTo(toSearch[start]);
        while (Double.compare(slope, origin.slopeTo(toSearch[j])) == 0) {
          j++;
          if (j >= toSearch.length) break;
        }

        if (j - start >= 3) {
          int subSegment = isSubSegment(subSegments, origin, toSearch[start]);

          if (subSegment >= 0) {
            subSegments.remove(subSegment + 1);
            subSegments.remove(subSegment);
          } else {
            for (int k = start; k < j - 3; k++) {
              subSegments.add(toSearch[k]);
              subSegments.add(toSearch[k + 1]);
            }

            segmentsList.add(new LineSegment(origin, toSearch[j - 1]));
          }
        }
        start = j;
      }
    }

    segments = segmentsList.toArray(new LineSegment[segmentsList.size()]);
  }

  public int numberOfSegments() {
    return segments.length;
  }

  public LineSegment[] segments() {
    return segments.clone();
  }

  private int isSubSegment(ArrayList<Point> arrayList, Point from, Point to) {
    for (int i = 0; i < arrayList.size(); i += 2) {
      if (arrayList.get(i).equals(from) && arrayList.get(i + 1).equals(to)) {
        return i;
      }
    }
    return -1;
  }

  public static void main(String[] args) {
    // read the n points from a file
    In in = new In(BruteCollinearPoints.class.getClassLoader().getResource("test.txt"));
    // In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
    }

    // draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();

    // print and draw the line segments
    FastCollinearPoints collinear = new FastCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdOut.println(collinear.numberOfSegments());
    StdDraw.show();
  }
}
