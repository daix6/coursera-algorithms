package week2;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
  private Item[] queues;
  private int size;

  public RandomizedQueue() {
    queues = (Item[])new Object[2];
    size = 0;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public int size() {
    return size;
  }

  public void enqueue(Item item) {
    if (item == null) {
      throw new IllegalArgumentException();
    }

    queues[size++] = item;

    if (size >= queues.length * 3 / 4) {
      resize(queues.length * 2);
    }
  }

  public Item dequeue() {
    if (size == 0) {
      throw new NoSuchElementException();
    }

    int index = StdRandom.uniform(size);
    final Item item = queues[index];
    queues[index] = queues[--size];
    queues[size] = null;

    if (size <= queues.length / 4) {
      resize(queues.length / 2);
    }

    return item;
  }

  public Item sample() {
    if (size == 0) {
      throw new NoSuchElementException();
    }

    return queues[StdRandom.uniform(size)];
  }

  private void resize(int target) {
    Item[] array = (Item[]) new Object[target];
    for (int i = 0; i < size; i++) {
      array[i] = queues[i];
    }
    queues = array;
  }

  public Iterator<Item> iterator() {
    return new RandomizedQueueIterator<>(queues, size);
  }

  private class RandomizedQueueIterator<Item> implements Iterator<Item> {
    private Item[] array;
    private int current;

    public RandomizedQueueIterator(Item[] queues, int size) {
      array = (Item[])new Object[size];
      current = 0;

      for (int i = 0; i < size; i++) {
        array[i] = queues[i];
      }

      if (size > 0) {
        for (int i = 0, idx; i < size; i++) {
          idx = StdRandom.uniform(size - i);
          Item temp = array[idx];
          array[idx] = array[size - i - 1];
          array[size - i - 1] = temp;
        }
      }
    }

    @Override
    public boolean hasNext() {
      return current < size;
    }

    @Override
    public Item next() {
      if (hasNext()) {
        return array[current++];
      }

      throw new NoSuchElementException();
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  public static void main(String[] args) {
    int[] count = new int[6];
    int[] sample = new int[3];
    RandomizedQueue<String> queue = new RandomizedQueue<String>();
    queue.enqueue("A");
    queue.enqueue("B");
    queue.enqueue("C");
    for (int i = 0; i < 1000; i++) {
      StringBuilder stringBuilder = new StringBuilder();
      for (String str : queue) {
        stringBuilder.append(str);
      }

      if (stringBuilder.toString().equals("ABC")) {
        count[0]++;
      } else if (stringBuilder.toString().equals("ACB")) {
        count[1]++;
      } else if (stringBuilder.toString().equals("BAC")) {
        count[2]++;
      } else if (stringBuilder.toString().equals("BCA")) {
        count[3]++;
      } else if (stringBuilder.toString().equals("CAB")) {
        count[4]++;
      } else if (stringBuilder.toString().equals("CBA")) {
        count[5]++;
      }
    }

    for (int i = 0; i < 6; i++) {
      System.out.println(count[i]);
    }

    Iterator<String> iterator1 = queue.iterator();
    Iterator<String> iterator2 = queue.iterator();
    while (iterator1.hasNext()) {
      System.out.print(iterator1.next());
    }
    while (iterator2.hasNext()) {
      System.out.print(iterator2.next());
    }
  }
}
