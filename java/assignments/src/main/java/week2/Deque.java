package week2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
  private class Node<Item> {
    private Item item;
    private Node<Item> prev;
    private Node<Item> next;

    public Node(Item item, Node<Item> prev, Node<Item> next) {
      this.item = item;
      this.prev = prev;
      this.next = next;
    }
  }

  private Node<Item> head;
  private Node<Item> tail;
  private int size;

  public Deque() {
    head = null;
    tail = null;
    size = 0;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public int size() {
    return size;
  }

  public void addFirst(Item item) {
    if (item == null) {
      throw new IllegalArgumentException();
    }

    Node<Item> newHead = new Node<Item>(item, null, head);

    if (isEmpty()) {
      head = newHead;
      tail = head;
    } else {
      head.prev = newHead;
      head = newHead;
    }

    size++;
  }

  public void addLast(Item item) {
    if (item == null) {
      throw new IllegalArgumentException();
    }

    Node<Item> newTail = new Node<Item>(item, tail, null);
    if (isEmpty()) {
      head = newTail;
      tail = head;
    } else {
      tail.next = newTail;
      tail = newTail;
    }
    size++;
  }

  public Item removeFirst() {
    if (isEmpty()) {
      throw new NoSuchElementException();
    }

    final Item item = head.item;

    if (head.next == null) {
      head = null;
      tail = head;
    } else {
      head = head.next;
      head.prev = null;
    }
    size--;

    return item;
  }

  public Item removeLast() {
    if (isEmpty()) {
      throw new NoSuchElementException();
    }

    final Item item = tail.item;
    if (tail.prev == null) {
      head = null;
      tail = head;
    } else {
      tail = tail.prev;
      tail.next = null;
    }
    size--;

    return item;
  }

  public Iterator<Item> iterator() {
    return new DequeIterator<>(head);
  }

  private class DequeIterator<Item> implements Iterator<Item> {
    private Node<Item> current;

    public DequeIterator(Node<Item> head) {
      current = head;
    }

    @Override
    public Item next() {
      if (hasNext()) {
        final Item item = current.item;
        current = current.next;
        return item;
      }

      throw new NoSuchElementException();
    }

    @Override
    public boolean hasNext() {
      return current != null;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
}
