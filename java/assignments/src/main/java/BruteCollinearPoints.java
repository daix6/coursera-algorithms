import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;

public class BruteCollinearPoints {
  private LineSegment[] segments;

  public BruteCollinearPoints(Point[] points) {
    if (points == null) throw new IllegalArgumentException();

    for (int i = 0; i < points.length; i++) {
      if (points[i] == null) throw new IllegalArgumentException();
      for (int j = i + 1; j < points.length; j++) {
        if (points[j] == null) throw new IllegalArgumentException();

        if (points[j].compareTo(points[i]) == 0) throw new IllegalArgumentException();
      }
    }

    ArrayList<LineSegment> segmentsList = new ArrayList<LineSegment>();
    double ij, jp, pq;
    Point min, max;

    for (int i = 0; i < points.length; i++) {
      for (int j = i + 1; j < points.length; j++) {
        ij = points[i].slopeTo(points[j]);
        for (int p = j + 1; p < points.length; p++) {
          jp = points[j].slopeTo(points[p]);
          if (ij != jp) continue;
          for (int q = p + 1; q < points.length; q++) {
            pq = points[p].slopeTo(points[q]);

            if (jp == pq) {
              int[] arr = {j, p, q};
              max = points[i];
              min = max;
              for (int k = 0; k < arr.length; k++) {
                if (max.compareTo(points[arr[k]]) < 0) {
                  max = points[arr[k]];
                }
                if (min.compareTo(points[arr[k]]) > 0) {
                  min = points[arr[k]];
                }
              }
              segmentsList.add(new LineSegment(min, max));
            }
          }
        }
      }
    }

    segments = segmentsList.toArray(new LineSegment[segmentsList.size()]);
  }

  public int numberOfSegments() {
    return segments.length;
  }

  public LineSegment[] segments() {
    return segments.clone();
  }

  public static void main(String[] args) {
    // read the n points from a file
    // In in = new In(BruteCollinearPoints.class.getClassLoader().getResource("test.txt"));
    In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
    }

    // draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();

    // print and draw the line segments
    BruteCollinearPoints collinear = new BruteCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdOut.println(collinear.numberOfSegments());
    StdDraw.show();
  }
}
