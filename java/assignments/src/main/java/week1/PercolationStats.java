package week1;

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
  private double[] stats;
  private static final double CONFIDENCE = 1.96;
  private final double mean;
  private final double stddev;

  public PercolationStats(int n, int trials) {
    if (n <= 0 || trials <= 0) {
      throw new IllegalArgumentException();
    }
    stats = new double[trials];

    Percolation percolation;
    for (int i = 0; i < trials; i++) {
      percolation = new Percolation(n);

      while (!percolation.percolates()) {
        percolation.open(StdRandom.uniform(1, n + 1), StdRandom.uniform(1, n + 1));
      }
      stats[i] = 1. * percolation.numberOfOpenSites() / (n * n);
    }
    mean = StdStats.mean(stats);
    stddev = StdStats.stddev(stats);
  }

  public double mean() {
    return mean;
  }

  public double stddev() {
    return stddev;
  }

  public double confidenceLo() {
    return mean - CONFIDENCE * stddev / Math.sqrt(stats.length);
  }

  public double confidenceHi() {
    return mean + CONFIDENCE * stddev / Math.sqrt(stats.length);
  }

  public static void main(String[] args) {
    int n = Integer.parseInt(args[0]);
    int trials = Integer.parseInt(args[1]);
    PercolationStats ps = new PercolationStats(n, trials);
    System.out.printf("mean                     = %f%n", ps.mean());
    System.out.printf("stddev                   = %f%n", ps.stddev());
    System.out.printf("95%% confidence interval = [%f, %f]%n", ps.confidenceLo(), ps.confidenceHi());
  }
}