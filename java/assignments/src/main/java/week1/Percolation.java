package week1;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
  private final int n;
  private boolean[] sites; // Sites container
  private final WeightedQuickUnionUF uf;
  private final WeightedQuickUnionUF ufTop;

  private final int top;
  private final int bottom;

  public Percolation(int n) {
    if (n <= 0) {
      throw new IllegalArgumentException();
    }

    this.n = n;
    top = 0;
    bottom = n * n + 1;
    sites = new boolean[n * n];
    for (int i = 0; i < n * n; i++) {
      sites[i] = false;
    }

    ufTop = new WeightedQuickUnionUF(n * n + 1);
    uf = new WeightedQuickUnionUF(n * n + 2);
  }

  /**
   * Open site at (row, col), start from (1, 1).
   */
  public void open(int row, int col) {
    validateCoordinates(row, col);

    if (isOpen(row, col)) {
      return;
    }

    int index = getIndex(row, col);
    if (row == 1) {
      ufTop.union(index, top);
      uf.union(index, top);
    }
    if (row == n) {
      uf.union(index, bottom);
    }

    // top
    if (row - 1 >= 1 && isOpen(row - 1, col)) {
      ufTop.union(index, index - n);
      uf.union(index, index - n);
    }
    // bottom
    if (row + 1 <= n && isOpen(row + 1, col)) {
      ufTop.union(index, index + n);
      uf.union(index, index + n);
    }
    // Left
    if (col - 1 >= 1 && isOpen(row, col - 1)) {
      ufTop.union(index, index - 1);
      uf.union(index, index - 1);
    }
    // Right
    if (col + 1 <= n && isOpen(row, col + 1)) {
      ufTop.union(index, index + 1);
      uf.union(index, index + 1);
    }

    // index - 1 because there are not top and bottom in sites.
    sites[index - 1] = true;
  }

  public boolean isOpen(int row, int col) {
    int index = getIndex(row, col);
    return sites[index - 1];
  }

  public boolean isFull(int row, int col) {
    validateCoordinates(row, col);
    return ufTop.connected(getIndex(row, col), top);
  }

  public int numberOfOpenSites() {
    int nums = 0;
    for (boolean i : sites) {
      if (i) {
        nums++;
      }
    }
    return nums;
  }

  public boolean percolates() {
    return uf.connected(top, bottom);
  }

  private void validateCoordinates(int row, int col) {
    if (row < 1 || col < 1 || row > n || col > n) {
      throw new IllegalArgumentException();
    }
  }

  private int getIndex(int row, int col) {
    validateCoordinates(row, col);
    return (row - 1) * n + col;
  }
}