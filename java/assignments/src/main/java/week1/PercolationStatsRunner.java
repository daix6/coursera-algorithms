package week1;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

public class PercolationStatsRunner {

  public static void runAnalysis() {
    try {
      int n, trials;

      StdOut.println(" N    | Trials | Time");
      for (n = 1; n <= 4096; n *= 2) {
        for (trials = 1; trials <= 10; trials++) {
        Stopwatch stopwatch = new Stopwatch();
        new PercolationStats(n, trials);
        StdOut.printf("% 6d|% 8d| %fs%n", n, trials, stopwatch.elapsedTime());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  };

  public static void run() {
    try {
      PercolationStats ps;
      In file = new In(PercolationStats.class.getClassLoader().getResource("percolations.in").getPath());
      while (!file.isEmpty()) {
        int p = file.readInt(), q = file.readInt();
        System.out.printf("%d %d%n", p, q);
        ps = new PercolationStats(p, q);
        System.out.printf("mean                     = %f%n", ps.mean());
        System.out.printf("stddev                   = %f%n", ps.stddev());
        System.out.printf("95%% confidence interval = [%f, %f]%n", ps.confidenceLo(), ps.confidenceHi());
      }
      file.close();
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }


  public static void main(String[] args) {
    PercolationStatsRunner.runAnalysis();
  }
}
